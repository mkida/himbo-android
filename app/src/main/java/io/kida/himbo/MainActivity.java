package io.kida.himbo;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.kida.himbo.intro.IntroAnimator;
import io.kida.himbo.intro.listener.OnIntroStateChangedListener;
import io.kida.himbo.sharing.WallpaperSharer;

import static io.kida.himbo.sharing.WallpaperSharer.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends Activity implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    @BindView(R.id.mainView)
    ViewGroup mMainView;

    private GestureDetector mGestureDetector;
    private IntroAnimator mIntro;
    private WallpaperSharer mWallpaperSharer;

    private float mHue;
    private float mSaturation;
    private float mBrightness;
    private float[] mLastPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mLastPoint = new float[]{};
        mGestureDetector = new GestureDetector(this, this);
        mIntro = new IntroAnimator(this, mMainView);
        mWallpaperSharer = new WallpaperSharer(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mMainView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!mIntro.getIntroInProgress()) {
                    mGestureDetector.onTouchEvent(motionEvent);
                    setColorAtPoint(motionEvent.getX(), motionEvent.getY());
                }
                return true;
            }
        });
    }

    private void setColorAtPoint(final float x, final float y) {
        if (mLastPoint.length == 2) {
            float abs = Math.abs(y - mLastPoint[1]);
            if (abs > 5 && abs < 100) {
                mHue = hueFormula(mMainView.getHeight(), y);
            }
        }

        if (y <= mMainView.getHeight() / 2) {
            mSaturation = saturationBrightnessFormula(mMainView.getWidth(), x);
        } else {
            mBrightness = saturationBrightnessFormula(mMainView.getWidth(), x);
        }

        mMainView.setBackgroundColor(getHsvColor());
        mLastPoint = new float[]{x, y};
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        final Context context = this;
        if (!mIntro.getIntroPlayed()) {
            mIntro.setOnIntroStateChangedListener(new OnIntroStateChangedListener() {
                @Override
                public void onAnimationUpdate(View view) {
                    setColorAtPoint(view.getX(), view.getY());
                }

                @Override
                public void onIntroFinished() {
                    Snackbar.make(mMainView, R.string.double_tap_to_save, Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    
                                }
                            })
                            .setActionTextColor(ContextCompat.getColor(context, R.color.white))
                            .show();
                }
            });
            setColorAtPoint(mIntro.getPulsingLayer().getX(), mIntro.getPulsingLayer().getY());
            mIntro.playIntroAnimation();
        }
    }

    private int getHsvColor() {
        return Color.HSVToColor(new float[]{
                mHue,
                mSaturation,
                mBrightness
        });
    }

    private float hueFormula(float x, float y) {
        return (360 / x) * (x - y);
    }

    private float saturationBrightnessFormula(float x, float y) {
        return (1 / x) * (x - y);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
        return false;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        mWallpaperSharer.shareImageForSolidColor(getHsvColor());
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mWallpaperSharer.shareImageForSolidColor(getHsvColor());
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.note)
                            .setMessage(R.string.write_permission_required)
                            .setNeutralButton(R.string.ok, null)
                            .show();
                }
            }
        }
    }
}
