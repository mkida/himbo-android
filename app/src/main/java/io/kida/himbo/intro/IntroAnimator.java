package io.kida.himbo.intro;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorUpdateListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.kida.himbo.R;
import io.kida.himbo.intro.listener.OnIntroStateChangedListener;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

public class IntroAnimator extends LinearLayout {
    @BindView(R.id.mainView)
    RelativeLayout mMainView;

    @BindView(R.id.pulsingLayer)
    PulsatorLayout mPulsingLayer;

    @BindView(R.id.infoLayer)
    LinearLayout mInfoLayer;

    private boolean mIntroPlayed = false;
    private boolean mIntroInProgress = false;

    private ViewGroup mViewGroup;
    private OnIntroStateChangedListener mOnIntroStateChangedListener;

    public IntroAnimator(Context context, ViewGroup viewGroup) {
        super(context);
        View.inflate(context, R.layout.intro, this);
        ButterKnife.bind(this);
        mViewGroup = viewGroup;
        mInfoLayer.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mInfoLayer.getVisibility() == View.VISIBLE) {
                    mInfoLayer.setVisibility(View.GONE);
                    mIntroInProgress = false;
                    if (mOnIntroStateChangedListener != null) {
                        mOnIntroStateChangedListener.onIntroFinished();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public boolean getIntroPlayed() {
        return mIntroPlayed;
    }

    public boolean getIntroInProgress() {
        return mIntroInProgress;
    }

    public PulsatorLayout getPulsingLayer() {
        return mPulsingLayer;
    }

    public void setOnIntroStateChangedListener(OnIntroStateChangedListener listener) {
        mOnIntroStateChangedListener = listener;
    }

    public void playIntroAnimation() {
        if (mIntroPlayed) {
            return;
        }
        if (getParent() == null) {
            setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            mViewGroup.addView(this);
        }
        mPulsingLayer.start();
        mIntroPlayed = true;
        mIntroInProgress = true;
        ViewCompat.animate(mPulsingLayer)
                .setUpdateListener(new ViewPropertyAnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(View view) {
                        if (mOnIntroStateChangedListener != null) {
                            mOnIntroStateChangedListener.onAnimationUpdate(view);
                        }
                    }
                })
                .setStartDelay(1000)
                .setDuration(1000)
                .x(mViewGroup.getWidth() / 2)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        mPulsingLayer.animate()
                                .setStartDelay(500)
                                .setDuration(2000)
                                .x(100)
                                .y(100)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        mPulsingLayer.animate()
                                                .setStartDelay(500)
                                                .setDuration(2000)
                                                .x(mViewGroup.getWidth() - mPulsingLayer.getWidth())
                                                .withEndAction(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        mPulsingLayer.stop();
                                                        mPulsingLayer.setVisibility(View.GONE);
                                                        mInfoLayer.setVisibility(View.VISIBLE);
                                                    }
                                                });
                                    }
                                });
                    }
                });
    }
}
