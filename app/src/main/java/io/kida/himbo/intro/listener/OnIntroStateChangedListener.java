package io.kida.himbo.intro.listener;

import android.view.View;

public interface OnIntroStateChangedListener {
    void onAnimationUpdate(View view);
    void onIntroFinished();
}
