package io.kida.himbo.sharing;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Display;

import io.kida.himbo.R;

public class WallpaperSharer {
    public final static int WRITE_EXTERNAL_STORAGE = 1000;
    private final static String WALLPAPER_FILENAME = "himbo-wallpaper";

    private Activity mActivity;
    private AlertDialog mShareAlert;
    private int mColor;

    public WallpaperSharer(Activity activity) {
        mActivity = activity;
    }

    // Share Image
    public void shareImageForSolidColor(int color) {
        mShareAlert = new AlertDialog.Builder(mActivity)
                .setCancelable(false)
                .setMessage(R.string.preparing_to_share)
                .show();
        mColor = color;

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                String path = getStoredPath(mColor);
                if (path == null) {
                    return;
                }
                shareImage(path);
            }
        });
    }

    private void shareImage(String path) {
        final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
        intent.setType("image/png");
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mActivity.startActivity(intent);
                mShareAlert.dismiss();
            }
        });
    }

    @Nullable
    private String getStoredPath(int color) {
        if (ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE
            );
            return null;
        }
        return storeImage(color);
    }

    private Bitmap getBitmap(int color) {
        Point size = getDisplaySize();
        Bitmap bitmap = Bitmap.createBitmap(size.x, size.y, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(color);
        return bitmap;
    }

    private Point getDisplaySize() {
        Display display = mActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    private String storeImage(int color) {
        return MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), getBitmap(color), WALLPAPER_FILENAME, null);
    }
}
